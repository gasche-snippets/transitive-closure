.PHONY: clean check bench

check: prog
	@for test in isolated chain cycle complete-shared; do \
	  echo -n "$$test: " ; ./prog compare $$test 10 ; \
	done

prog: transitive_closure.ml
	ocamlopt -o prog transitive_closure.ml

clean:
	rm prog
	rm -f transitive_closure.cm* transitive_closure.o

HYPER2=hyperfine -L impl new,again --command-name {impl}
HYPER3=hyperfine -L impl old,new,again --command-name {impl}
OUTDIR=hyperfine-output

bench: prog
	mkdir -p ${OUTDIR}
	${HYPER3} --export-markdown ${OUTDIR}/isolated.md "./prog {impl} isolated 100_000"
	${HYPER3} --export-markdown ${OUTDIR}/cycle.md "./prog {impl} cycle 300"
	${HYPER3} --export-markdown ${OUTDIR}/complete.md "./prog {impl} complete-shared 150"


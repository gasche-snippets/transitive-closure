This is a small benchmark of transitive-closure computation algorithms
for use in the OCaml compiler, originally written by Florian Weimer
and extended by Gabriel Scherer.

See https://github.com/ocaml/ocaml/pull/13150 for context.

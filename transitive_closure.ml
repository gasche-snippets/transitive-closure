module Variable = struct
  module Pair = struct
    module Set = Set.Make(Int)
    module Map = Map.Make(Int)
  end
end

type t =
  | Top
  | Implication of Variable.Pair.Set.t

type top_rel = t Variable.Pair.Map.t

let t_equal a b =
  match (a, b) with
  | (Top, Top) -> true
  | (Top, _) | (_, Top) -> false
  | (Implication a, Implication b) -> Variable.Pair.Set.equal a b

let t_union s1 s2 =
  match s1, s2 with
  | Top, _ | _, Top -> Top
  | Implication s1, Implication s2 ->
    Implication (Variable.Pair.Set.union s1 s2)

let transitive_closure_old (state : top_rel) =
  let union = t_union in
  let equal s1 s2 =
    match s1, s2 with
    | Top, Implication _ | Implication _, Top -> false
    | Top, Top -> true
    | Implication s1, Implication s2 -> Variable.Pair.Set.equal s1 s2
  in
  let update arg state =
    let original_set =
      try Variable.Pair.Map.find arg state with
      | Not_found -> Implication Variable.Pair.Set.empty
    in
    match original_set with
    | Top -> state
    | Implication arguments ->
        let set =
          Variable.Pair.Set.fold
            (fun orig acc->
               let set =
                 try Variable.Pair.Map.find orig state with
                 | Not_found -> Implication Variable.Pair.Set.empty in
               union set acc)
            arguments original_set
        in
        Variable.Pair.Map.add arg set state
  in
  let once state =
    Variable.Pair.Map.fold (fun arg _ state -> update arg state) state state
  in
  let rec fp state =
    let state' = once state in
    if Variable.Pair.Map.equal equal state state'
    then state
    else fp state'
  in
  fp state

let transitive_closure_new (state : top_rel) =
  (* Depth-first search for all implications for one argument.
     Arguments are moved from candidate to frontier, assuming
     they are newly added to the result. *)
  let rec loop candidate frontier result =
    match (candidate, frontier) with
    | ([], []) -> Implication result
    | ([], frontier::fs) ->
      (* Obtain fresh candidate for the frontier argument. *)
      (match Variable.Pair.Map.find frontier state with
       | exception Not_found -> loop [] fs result
       | Top -> Top
       | Implication candidate ->
         loop (Variable.Pair.Set.elements candidate) fs result)
    | (candidate::cs, frontier) ->
      let result' = Variable.Pair.Set.add candidate result in
      if result' != result then
        (* Result change means candidate becomes part of frontier. *)
        loop cs (candidate :: frontier) result'
      else
        loop cs frontier result
  in
    Variable.Pair.Map.map
      (fun set ->
         match set with
	 | Top -> Top
	 | Implication set -> loop [] (Variable.Pair.Set.elements set) set)
      state

let transitive_closure_again (rel : top_rel) : top_rel =
  (* [state] contains the transitive closure of some elements of [rel]
     that we have already visited. *)
  let state = ref Variable.Pair.Map.empty in
  (* [loop frontier result] computes the union of [result] with the
     reflexive-transitive closure of the nodes of [frontier]. In
     particular, if [ws] are the children of [v], then [loop [ws]
     empty] is the transitive closure of [v].

     This function works correctly on cyclic relations, but it
     performs better if the children of a node are already in [state].

     Invariant: each node in [result] either has all its children
     in [result] or is included in the [frontier]. *)
  let rec loop frontier result =
    match frontier with
    | [] -> Implication result
    | []::frontier -> loop frontier result
    | (v::vs)::frontier ->
      let frontier = vs::frontier in
      let old_result = result in
      let result = Variable.Pair.Set.add v result in
      if result == old_result then
        (* Already in the result. *)
        loop frontier result
      else begin
        (* First look a whether we have already computed the closure of [v]. *)
        match Variable.Pair.Map.find v !state with
        | Top -> Top
        | Implication clos ->
          (* [clos] is closed, there are no new nodes reachable from it:
             no need to grow the frontier. *)
          loop frontier (Variable.Pair.Set.union result clos)
        | exception Not_found ->
        (* Otherwise we grow the frontier with the children of [v]. *)
        match Variable.Pair.Map.find v rel with
        | exception Not_found -> loop frontier result
        | Top -> Top
        | Implication ws ->
          loop (Variable.Pair.Set.elements ws :: frontier) result
      end
  in
  (* [visit v] computes the transitive closure of [v]
     in reverse topological order, using [loop] on cyclic subgraphs. *)
  let visited = ref Variable.Pair.Set.empty in
  let rec visit v =
    match Variable.Pair.Map.find v !state with
    | t -> t
    | exception Not_found ->
      (* We have not computed the closure of this element yet *)
      let clos =
        match Variable.Pair.Map.find v rel with
        | Top -> Top
        | exception Not_found -> Implication Variable.Pair.Set.empty
        | Implication ws ->
          let cyclic =
            (* reflexive relations have (v -> v), we check this common
               case which does not need the larger !visited set *)
            Variable.Pair.Set.mem v ws
            || Variable.Pair.Set.mem v !visited
          in
          if cyclic then
            loop [Variable.Pair.Set.elements ws] Variable.Pair.Set.empty
          else begin
            (* if there is no cycle, visit the children first *)
            visited := Variable.Pair.Set.add v !visited;
            Variable.Pair.Set.fold (fun w acc ->
              acc
              |> t_union (visit w)
            ) ws (Implication ws)
          end
      in
      state := Variable.Pair.Map.add v clos !state;
      clos
  in
  Variable.Pair.Map.mapi (fun v _ -> visit v) rel

let rel_isolated n =
  Variable.Pair.Map.of_seq
    (Seq.init n (fun i -> (i, Implication (Variable.Pair.Set.singleton i))))

let rel_chain n =
  Variable.Pair.Map.of_seq
    (Seq.init n (fun i ->
       (i, if i = (n - 1)
           then Implication (Variable.Pair.Set.singleton i)
	   else Implication (Variable.Pair.Set.singleton (i + 1)))))

let rel_cycle n =
  Variable.Pair.Map.of_seq
    (Seq.init n (fun i ->
       (i, if i = (n - 1)
           then Implication (Variable.Pair.Set.singleton 0)
	   else Implication (Variable.Pair.Set.singleton (i + 1)))))

let rel_complete n =
  Variable.Pair.Map.of_seq
    (Seq.init n (fun i ->
                 (i, Implication
		     (Variable.Pair.Set.of_seq (Seq.init n (fun i -> i))))))

let rel_complete_shared n =
  let s = Implication (Variable.Pair.Set.of_seq (Seq.init n (fun i -> i))) in
  Variable.Pair.Map.of_seq
    (Seq.init n (fun i -> (i, s)))

let print_statistics rel =
  let nodes = Variable.Pair.Map.cardinal rel in
  let (top, edges) = Variable.Pair.Map.fold
    (fun _ set  (top, edges)-> match set with
                  | Top -> (top + 1, edges)
		  | Implication set ->
		    (top, edges + (Variable.Pair.Set.cardinal set)))
     rel (0, 0)
   in Printf.printf "nodes=%d top=%d edges=%d\n" nodes top edges

let mk_op tc rel =
  print_statistics (tc rel)

let compare_old_again rel =
  let closure_old = transitive_closure_old rel in
  let closure_new = transitive_closure_again rel in
  if Variable.Pair.Map.equal t_equal closure_old closure_new then
    Printf.printf "match\n"
  else
    Printf.printf "failure\n"

let usage () =
  Printf.fprintf stderr
    "usage: %s {old|new|again|compare} RELATION COUNT\n"
    Sys.argv.(0);
  Printf.fprintf stderr
    "  RELATION: one of isolated chain cycle complete complete-shared\n";
  exit 1

let to_op op =
  match op with
  | "old" -> mk_op transitive_closure_old
  | "new" -> mk_op transitive_closure_new
  | "again" -> mk_op transitive_closure_again
  | "compare" -> compare_old_again
  | _ -> usage ()

let to_rel rel =
  match rel with
  | "isolated" -> rel_isolated
  | "chain" -> rel_chain
  | "cycle" -> rel_cycle
  | "complete" -> rel_complete
  | "complete-shared" -> rel_complete_shared
  | _ -> usage ()

let () = match Sys.argv with
  | [|_; op; rel; count|] ->
      let op = to_op op in
      let rel = to_rel rel in
      let count = int_of_string count in
      op (rel count)
  | _ -> usage ()
